package com.polat.todolist;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.polat.todolist.adapter.RecyclerViewAdapter;
import com.polat.todolist.databinding.ActivityMainBinding;
import com.polat.todolist.model.TodoDto;
import com.polat.todolist.service.TodoService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ArrayList<TodoDto> list;
    private String BASE_URL = "https://jsonplaceholder.typicode.com/";
    Retrofit retrofit;
    ActivityMainBinding binding;
    CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Gson gson = new GsonBuilder().setLenient().create();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                //rxJava kullanıyoruz
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        loadData();

    }

    private void loadData(){

        final TodoService todoService = retrofit.create(TodoService.class);

        compositeDisposable = new CompositeDisposable();

        compositeDisposable.add(todoService.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResponse));

    }

    private void handleResponse(List<TodoDto> todoDtoList) {
        list = new ArrayList<>(todoDtoList);
        binding.recyclerView.setAdapter(new RecyclerViewAdapter(list));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        compositeDisposable.clear();
    }
}