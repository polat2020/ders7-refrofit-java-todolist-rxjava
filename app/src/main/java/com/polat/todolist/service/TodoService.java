package com.polat.todolist.service;

import com.polat.todolist.model.TodoDto;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;

public interface TodoService {

    @GET("todos")
    Observable<List<TodoDto>> getData();

}
