package com.polat.todolist.adapter;

import androidx.recyclerview.widget.RecyclerView;

import com.polat.todolist.databinding.RowLayoutBinding;

public class RowHolder extends RecyclerView.ViewHolder {
    RowLayoutBinding binding;
    public RowHolder(RowLayoutBinding binding) {
        super(binding.getRoot());
        this.binding= binding;
    }
}